package sbu.cs;

import sbu.cs.app.Machine;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Machine machine=new Machine(arr,n);
        return machine.exe(input);
    }
}
