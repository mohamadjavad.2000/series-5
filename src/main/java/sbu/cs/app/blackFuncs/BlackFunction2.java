package sbu.cs.app.blackFuncs;

import sbu.cs.app.interfaces.BlackFunction;

public class BlackFunction2 implements BlackFunction {
    @Override
    public String func(String arg) {
        StringBuilder repeat = new StringBuilder();
        for (int i=0;i<arg.length();i++)
            repeat.append(arg.charAt(i)).append(arg.charAt(i));
        return repeat.toString();
    }
}
