package sbu.cs.app.blackFuncs;

import sbu.cs.app.interfaces.BlackFunction;

public class BlackFunction1 implements BlackFunction {
    @Override
    public String func(String arg) {
        StringBuilder reverse =new StringBuilder(arg);
        reverse.reverse();
        return reverse.toString();
    }
}
