package sbu.cs.app.blackFuncs;

import sbu.cs.app.interfaces.BlackFunction;

public class BlackFunction4 implements BlackFunction {
    @Override
    public String func( String arg) {
        int lastIndex=arg.length()-1;
        if (lastIndex<1)
            return arg;
        return arg.charAt(lastIndex)+arg.substring(0,lastIndex);
    }
}
