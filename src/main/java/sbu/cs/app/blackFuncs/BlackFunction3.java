package sbu.cs.app.blackFuncs;

import sbu.cs.app.interfaces.BlackFunction;

public class BlackFunction3 implements BlackFunction {
    @Override
    public String func(String arg) {
        return arg + arg;
    }
}
