package sbu.cs.app.blackFuncs;

import sbu.cs.app.interfaces.BlackFunction;

public class BlackFunction5 implements BlackFunction {
    @Override
    public String func(String arg) {
        StringBuilder zToA=new StringBuilder();
        char ch;
        for (int i=0;i<arg.length();i++){
            ch=arg.charAt(i);
            if ( ch>96){
                zToA.append((char)(219- arg.charAt(i)));
            }else{
                zToA.append((char) (155- arg.charAt(i)));
            }
        }
        return zToA.toString();
    }
}
