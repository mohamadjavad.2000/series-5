package sbu.cs.app.whiteFuncs;

import sbu.cs.app.interfaces.WhiteFunction;

public class WhiteFunction3 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0, j = arg2.length(); i < arg1.length() || j > -1; i++) {
            j--;
            if (i < arg1.length()) {
                sb.append(arg1.charAt(i));
            }
            if (j > -1) {
                sb.append(arg2.charAt(j));
            }
        }
        return sb.toString();
    }
}
