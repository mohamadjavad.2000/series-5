package sbu.cs.app.whiteFuncs;

import sbu.cs.app.interfaces.WhiteFunction;

public class WhiteFunction4 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        if (arg1.length() % 2 == 0)
            return arg1;
        return arg2;
    }
}
