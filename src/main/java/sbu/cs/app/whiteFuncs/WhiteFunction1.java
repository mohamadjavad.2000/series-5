package sbu.cs.app.whiteFuncs;

import sbu.cs.app.interfaces.WhiteFunction;

public class WhiteFunction1 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        StringBuilder str = new StringBuilder();
        int min = Integer.min(arg1.length(), arg2.length());
        int i = 0;
        for (; i < min; i++) str.append(arg1.charAt(i)).append(arg2.charAt(i));
        if (min < arg1.length()) {
            for (; i < arg1.length(); i++) str.append(arg1.charAt(i));
        } else if (min < arg2.length()) {
            for (; i < arg2.length(); i++) str.append(arg2.charAt(i));
        }
        return str.toString();
    }
}
