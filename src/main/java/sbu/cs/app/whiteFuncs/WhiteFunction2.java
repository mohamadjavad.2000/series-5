package sbu.cs.app.whiteFuncs;

import sbu.cs.app.interfaces.WhiteFunction;

public class WhiteFunction2 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        return arg1 + new StringBuilder(arg2).reverse();
    }
}
