package sbu.cs.app.whiteFuncs;

import sbu.cs.app.interfaces.WhiteFunction;

public class WhiteFunction5 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        StringBuilder sb = new StringBuilder();
        int min = Integer.min(arg1.length(), arg2.length());
        int i = 0, ch1, ch2;
        for (; i < min; i++) {
            ch1 = arg1.charAt(i);
            ch2 = arg2.charAt(i);
            sb.append((char) (97+(ch1 + ch2 -97*2) %26));
        }
        if (min < arg1.length()) {
            for (; i < arg1.length(); i++) sb.append(arg1.charAt(i));
        } else if (min < arg2.length()) {
            for (; i < arg2.length(); i++) sb.append(arg2.charAt(i));
        }
        return sb.toString();
    }
}
