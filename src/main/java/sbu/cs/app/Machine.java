package sbu.cs.app;

import sbu.cs.app.blackFuncs.*;
import sbu.cs.app.elements.*;
import sbu.cs.app.interfaces.BlackFunction;
import sbu.cs.app.interfaces.Element;
import sbu.cs.app.interfaces.WhiteFunction;
import sbu.cs.app.whiteFuncs.*;

public class Machine {
    private final int lastIndex;
    private ExitElement exit;
    private final Element[][] elements;
    private final WhiteFunction[] wFunctions =new WhiteFunction[6];
    private final BlackFunction[] bFunctions =new BlackFunction[6];

    /**
     * makes machine ready to use by creating its elements
     * @param arr array of sort of functions
     * @param n size of array
     */
    public Machine(int[][] arr, int n) {
        this.lastIndex =n-1;
        initFunctions();
        elements=new Element[n][n];
        for (int i=n-1;i>-1;i--){
            elements[i]=new Element[n];
            for (int j=n-1;j>-1;j--){
                produceElement(arr[i][j],i,j);
            }
        }
    }

    /**
     * produce suitable kind of element for the machine table
     * @param sort number of functions
     * @param i index of element
     * @param j index of element
     */
    private void produceElement(int sort, int i, int j) {
        if (i == 0||j==0) {
            if(i== lastIndex) {
                elements[i][j]= new YellowElement(bFunctions[sort],elements[i][1]);
            }else if (j== lastIndex){
                elements[i][j]= new YellowElement(bFunctions[sort],elements[1][j]);
            }else {
                elements[i][j]= new GreenElement(bFunctions[sort],elements[i][j+1],elements[i+1][j]);
            }
        }else if (!(i==lastIndex || j==lastIndex)){
            elements[i][j]=new BlueElement(bFunctions[sort],elements[i][j+1],elements[i+1][j]);
        }else {
            if (i==j){
                exit=new ExitElement();
                elements[i][j]=new PinkElement(wFunctions[sort],exit);
            }else if (i==lastIndex){
                elements[i][j]=new PinkElement(wFunctions[sort],elements[i][j+1]);
            }else {
                elements[i][j]=new PinkElement(wFunctions[sort],elements[i+1][j]);
            }
        }

    }

    /**
     * make black & white functions accessible by its sort number
     * & makes instance of all of funcs
     */
    private void initFunctions() {
        wFunctions[1]=new WhiteFunction1();
        wFunctions[2]=new WhiteFunction2();
        wFunctions[3]=new WhiteFunction3();
        wFunctions[4]=new WhiteFunction4();
        wFunctions[5]=new WhiteFunction5();
        bFunctions[1]=new BlackFunction1();
        bFunctions[2]=new BlackFunction2();
        bFunctions[3]=new BlackFunction3();
        bFunctions[4]=new BlackFunction4();
        bFunctions[5]=new BlackFunction5();
    }

    /**
     * executes machine
     * @param input of machine
     * @return machine output
     */
    public String exe(String input) {
        elements[0][0].exe(input,false);
        return exit.getMachineOutput();
    }
}
