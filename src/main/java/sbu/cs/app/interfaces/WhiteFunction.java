package sbu.cs.app.interfaces;

public interface WhiteFunction {
    public String func(String arg1, String arg2);
}
