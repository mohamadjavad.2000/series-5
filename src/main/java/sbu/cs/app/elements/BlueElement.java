package sbu.cs.app.elements;

import sbu.cs.app.interfaces.BlackFunction;
import sbu.cs.app.interfaces.Element;

public class BlueElement implements Element {
    private final BlackFunction blackFunction;
    private final Element down;
    private final Element right;

    public BlueElement (BlackFunction blackFunction, Element right, Element down) {
        this.blackFunction = blackFunction;
        this.down = down;
        this.right = right;
    }

    @Override
    public void exe(String str,boolean b){
        str=blackFunction.func(str);
        if (b) right.exe(str, true);
        else down.exe(str, false);
    }
}
