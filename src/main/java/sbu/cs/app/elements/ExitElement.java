package sbu.cs.app.elements;

import sbu.cs.app.interfaces.Element;

public class ExitElement implements Element {
    private String machineOutput;

    public String getMachineOutput() {
        return machineOutput;
    }

    @Override
    public void exe(String str,boolean left){
        machineOutput =str;
    }
}
