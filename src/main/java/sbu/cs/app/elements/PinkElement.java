package sbu.cs.app.elements;

import sbu.cs.app.interfaces.Element;
import sbu.cs.app.interfaces.WhiteFunction;

public class PinkElement implements Element {
    private final WhiteFunction whiteFunction;
    private final Element next;
    private boolean up=false;
    private String arg1;

    public PinkElement(WhiteFunction whiteFunction, Element next) {
        this.whiteFunction = whiteFunction;
        this.next = next;
    }
    @Override
    public void exe(String str,boolean left){
        if (up) {
            next.exe(whiteFunction.func(arg1,str),false);
        }else {
            arg1=str;
            up=true;
        }
    }
}
