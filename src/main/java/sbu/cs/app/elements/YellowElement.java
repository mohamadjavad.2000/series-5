package sbu.cs.app.elements;

import sbu.cs.app.interfaces.BlackFunction;
import sbu.cs.app.interfaces.Element;

public class YellowElement implements Element {
    private final BlackFunction blackFunction;
    private final Element next;

    public YellowElement(BlackFunction blackFunction, Element next) {
        this.blackFunction = blackFunction;
        this.next = next;
    }
    @Override
    public void exe(String str,boolean left){
        next.exe(blackFunction.func(str),false);
    }
}
