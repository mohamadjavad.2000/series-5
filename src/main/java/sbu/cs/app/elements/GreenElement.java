package sbu.cs.app.elements;

import sbu.cs.app.interfaces.BlackFunction;
import sbu.cs.app.interfaces.Element;

public class GreenElement implements Element {
    private final BlackFunction blackFunction;
    private final Element right;
    private final Element down;

    public GreenElement(BlackFunction blackFunction, Element right, Element down) {
        this.blackFunction = blackFunction;
        this.right = right;
        this.down = down;
    }
    @Override
    public void exe(String str,boolean left){
        str=blackFunction.func(str);
        down.exe(str,false);
        right.exe(str,true);
    }
}
