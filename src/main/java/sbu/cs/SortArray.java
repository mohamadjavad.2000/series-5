package sbu.cs;

import java.util.Arrays;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of array
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        int minIndex;
        for (int i=0; i<size; i++){
            minIndex=i;
            for (int j=i+1; j<size; j++){
                if (arr[minIndex]>arr[j]){
                    minIndex=j;
                }
            }
            swap(arr,minIndex,i);
        }
        return arr;
    }

    /** swap arr[index1] with arr[index2] */
    private void swap(int[] arr, int index1, int index2) {
        if (index1==index2){
            return;
        }
        int temp=arr[index1];
        arr[index1]=arr[index2];
        arr[index2]=temp;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i=0; i<size; i++){
            for (int j=i-1; 0<=j; j--){
                if (arr[j]<arr[j+1]){
                    break;
                }
                swap(arr,j,j+1);
            }
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of array
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size<2){
            return arr;
        }
        int mid =(int) Math.ceil(size/2.0);
        int[] arr1 = Arrays.copyOfRange(arr, 0, mid);
        int[] arr2 =Arrays.copyOfRange(arr, mid, size);
        mergeSort(arr1,mid);
        mergeSort(arr2,size/2);
        merge(arr,arr1,arr2);
        return arr;
    }

    /**
     * merge 2 sorted parts of array arr into arr
     * [get an unsorted array arr & its parts arr1&2 (which every one of this arrays is sorted divided)
     * then sort this 2 arrays' elements when coping them smallest by smallest into arr]
     *
     * @param arr unsorted array of integers
     * @param arr1 first sorted part
     * @param arr2 second sorted part
     */
    private void merge(int[] arr, int[] arr1, int[] arr2) {
        int c1=0,c2=0,i=0;
        while (i<arr.length) {
            if (c1<arr1.length && c2<arr2.length){
                if (arr1[c1]<arr2[c2]){
                    arr[i]=arr1[c1++];
                }
                else {
                    arr[i]=arr2[c2++];
                }
                i++;
            }
            else {
                if (c1<arr1.length){
                    for (;i<arr.length;i++){
                        arr[i]=arr1[i-c2];
                    }
                } else {
                    for (;i<arr.length;i++){
                        arr[i]=arr2[i-c1];
                    }
                }
            }
        }
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int begin=0, end=arr.length;
        int mid=arr.length/2;
        while (true){
            if (end==begin)
                return -1;
            if (arr[mid]<value){
                begin=++mid;
                mid=(end-begin)/2 + begin;
            }
            else if (arr[mid]>value){
                end=mid;
                mid=(end-begin)/2+begin;
            }
            else {
                return mid;
            }
        }
    }
    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        if (arr.length==0)
                return -1;
        int mid=arr.length/2;
        if (arr[mid]>value){
            return binarySearchRecursive(Arrays.copyOfRange(arr,0,mid),value);
        }
        if (arr[mid] < value) {
            int ans=binarySearchRecursive(Arrays.copyOfRange(arr, ++mid, arr.length), value);
            if (ans==-1)
                return -1;
            return ans+mid;
        }
        return mid;
    }
}
